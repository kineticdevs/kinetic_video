import datetime

from dateutil.parser import parse

from app import db


class Video(db.Model):
    __tablename__ = 'video'
    id = db.Column(db.Integer, primary_key=True)
    location = db.Column(db.String(64))
    day = db.Column(db.String(64))
    start_t = db.Column(db.String(255))
    end_t = db.Column(db.String(255))
    s3_key = db.Column(db.String(64))
    description = db.Column(db.Text)

    devices = db.relationship('VideoDevice', backref='video', lazy='dynamic')
    windows = db.relationship('VideoWindow', backref='video', lazy='dynamic')

    def get_location(self):
        return self.location

    def get_day(self):
        return '{}/{}/{}'.format(self.day[:4], self.day[4:6], self.day[6:8])

    def get_video_file(self):
        return self.s3_key.split('/')[-1]

    def get_devices_ids(self):
        return [str(d.device_id) for d in self.devices]

    def get_times(self):
        return '{} - {}'.format(self.start_t, self.end_t)

    def get_description(self):
        return self.description

    def has_been_sync(self):
        return not (self.start_t is None or self.end_t is None)

    def sync(self, current_video_time_ms, true_time, video_length_ms):
        """
        @param arg: UTC time input in format 2009/03/12 19:19:03
        @return:
        """
        dt = parse(true_time)
        timestamp_reference = (dt - datetime.datetime(1970, 1, 1)).total_seconds() * 1000
        print current_video_time_ms
        self.start_t = str(int(timestamp_reference - current_video_time_ms))
        self.end_t = str(int(self.start_t) + video_length_ms)

    def __repr__(self):
        return '<Video {} shot at {} on day {}>'.format(self.id, self.location, self.day)


class VideoDevice(db.Model):
    __tablename__ = 'video_device'
    id = db.Column(db.Integer, primary_key=True)
    device_id = db.Column(db.Integer)
    video_id = db.Column(db.Integer, db.ForeignKey('video.id'))

    windows = db.relationship('Window', backref='video_device', lazy='dynamic')

    def __repr__(self):
        return '<VideoDevice {} on Video {} and Device {}>'.format(self.id, self.video_id, self.device_id)


class Window(db.Model):
    __tablename__ = 'window'
    id = db.Column(db.Integer, primary_key=True)
    video_window_id = db.Column(db.Integer, db.ForeignKey('video_window.id'))
    video_device_id = db.Column(db.Integer, db.ForeignKey('video_device.id'))

    annotations = db.relationship('AnnotationWindow', backref='window', lazy='dynamic')

    def __repr__(self):
        return '<Window {}>'.format(self.id)

    def get_annotations_values(self, type='str'):
        annotations_values = [aw.annotation.value for aw in self.annotations]
        return annotations_values if type == 'list' else ','.join(annotations_values)


class VideoWindow(db.Model):
    __tablename__ = 'video_window'
    id = db.Column(db.Integer, primary_key=True)
    start_t = db.Column(db.Integer)
    end_t = db.Column(db.Integer)
    video_id = db.Column(db.Integer, db.ForeignKey('video.id'))
    detection_type = db.Column(db.String(64))

    window = db.relationship('Window', backref='video_window', lazy='dynamic')

    def __repr__(self):
        return '<Window {} from {} to {}>'.format(self.id, self.start_t, self.end_t)


class Annotation(db.Model):
    __tablename__ = 'annotation'
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.String(64), unique=True)

    windows = db.relationship('AnnotationWindow', backref='annotation', lazy='dynamic')

    def __repr__(self):
        return '<Annotation {} - {}>'.format(self.id, self.value)


class AnnotationWindow(db.Model):
    __tablename__ = 'annotation_window'
    id = db.Column(db.Integer, primary_key=True)
    window_id = db.Column(db.Integer, db.ForeignKey('window.id'))
    annotation_id = db.Column(db.Integer, db.ForeignKey('annotation.id'))

    def __repr__(self):
        return '<Annotation {} on window {} with value {}>'.format(self.id, self.window_id, self.annotation_id)

function activate_menu(page_name){
    $('nav ul li').removeClass('active');
    $('#nav-' + page_name).addClass('active');
}


function get_video_id(){
    return(parseInt($('#my-data').attr('video-id')));
}

$(document).ready(function(){

    var page_name = $('#main-container').attr('page-name');
    activate_menu(page_name);

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })


})
var video;

function check_true_time_validity(){
    return($('#true-time').val() != '');
}

function set_current_time(t){
    video[0].currentTime = t;
}

function play_beg_end(beg, end){
    video[0].currentTime = beg;
    if(video[0].paused){
        play_pause()
    }

    setTimeout(function(){
        if(!video[0].paused){
            play_pause();
        }
    }, (end-beg + 2) * 1000);
}

function play_pause(){
    var play_pause_elt = $('#control-play');
    if(video[0].paused) {
        video[0].play();
        play_pause_elt.find('.glyphicon').removeClass('glyphicon-play');
        play_pause_elt.find('.glyphicon').addClass('glyphicon-pause');
    }
    else {
        video[0].pause();
        play_pause_elt.find('.glyphicon').removeClass('glyphicon-pause');
        play_pause_elt.find('.glyphicon').addClass('glyphicon-play');
    }
}

function get_duration(rounded){
    var d = video[0].duration;
    if(rounded){d = d.toFixed(2);}
    return(d);
}

function get_current_time(rounded){
    var c = video[0].currentTime;
    if(rounded){c = c.toFixed(2);}
    return(c);
}

function mute_unmute(){
    var mute_unmute_elt = $('#control-volume');
    if(video[0].muted){
        mute_unmute_elt.find('.glyphicon').removeClass('glyphicon-volume-off');
        mute_unmute_elt.find('.glyphicon').addClass('glyphicon-volume-up');
    }else{
        mute_unmute_elt.find('.glyphicon').removeClass('glyphicon-volume-up');
        mute_unmute_elt.find('.glyphicon').addClass('glyphicon-volume-off');
    }
    video[0].muted = !video[0].muted;
    return false;
}

$(document).ready(function(){

    video = $('#s3Video');

    //Mute/Unmute control clicked
    $('#control-volume').click(function() {
        mute_unmute();
    });

    //Play/Pause control clicked
    $('#control-play').on('click', function() {
        play_pause();
        return false;
    });

    //update HTML5 video current play time
    video.on('timeupdate', function() {
        $('.current').text(get_current_time(true));
    });

    //get HTML5 video time duration
    video.on('loadedmetadata', function() {
        $('.duration').text(get_duration(true));
    });

    //update HTML5 video current play time
    video.on('timeupdate', function() {
        var currentPos = video[0].currentTime; //Get currenttime
        var maxduration = video[0].duration; //Get video duration
        var percentage = 100 * currentPos / maxduration; //in %
        $('.timeBar').css('width', percentage+'%');
    });

    var timeDrag = false;   /* Drag status */
    $('.progressBar').mousedown(function(e) {
        timeDrag = true;
        updatebar(e.pageX);
    });
    $(document).mouseup(function(e) {
        if(timeDrag) {
            timeDrag = false;
            updatebar(e.pageX);
        }
    });
    $(document).mousemove(function(e) {
        if(timeDrag) {
            updatebar(e.pageX);
        }
    });

    //update Progress Bar control
    var updatebar = function(x) {
        var progress = $('.progressBar');
        var maxduration = video[0].duration; //Video duraiton
        var position = x - progress.offset().left; //Click pos
        var percentage = 100 * position / progress.width();

        //Check within range
        if(percentage > 100) {
            percentage = 100;
        }
        if(percentage < 0) {
            percentage = 0;
        }

        //Update progress bar and video currenttime
        $('.timeBar').css('width', percentage+'%');
        video[0].currentTime = maxduration * percentage / 100;
    };

    $('#sync-video').click(function(){
        if(check_true_time_validity() == ''){return false};

        var data = {
        current_video_time_ms: Math.round(video[0].currentTime * 1000),
        true_time: $('#true-time').val(),
        video_length_ms: Math.round(video[0].duration * 1000),
        video_id: get_video_id()
        }
        console.log(data);

        $.post( "/sync", data)
            .done(function( data ) {
            location.reload();
            });
    });

})
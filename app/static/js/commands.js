var window_start = -1;
var devices_selected = [];
var current_window_start_end = {'beg': 0, 'end':0};

function add_annotation_row(tablerow){

    var row = '<tr>'
    row += '<td><button onclick="javascript:play_beg_end(' + tablerow.start_t + ',' + tablerow.end_t + ')" type="button" class="btn btn-default btn-sm annotation-play" data-toggle="tooltip" title="Play this window" play-beg="' + tablerow.start_t + '"play-end="' + tablerow.end_t + '"window-id="' + tablerow.window_id + '"> <span class="glyphicon glyphicon-play"></span></button></td>'
    row += '<td>' + tablerow.video_s3_key + '</td>'
    row += '<td>' + tablerow.start_t + '<span class="glyphicon glyphicon-arrow-right"></span> ' + tablerow.end_t + '</td>'
    row += '<td>' + tablerow.d_type + '</td>'
    row += '<td onclick="javascript:annotate(\'\', ' + tablerow.window_id + ')" class="add-annotation" id="annotations-window-' + tablerow.window_id + '" title="Annotate this window" window-id="' + tablerow.window_id + '">' + tablerow.annotations + '</td>'
    row += '<td>' + tablerow.device_id + '</td>'
    row += '</tr>';

    $('#annotations-table').prepend(row);

}

function check_annotation(ann){
    return(ann!=null)
}

function update_window_id(window_play_button){
    $('#repeat-video-window-id').html(window_play_button.attr('video-window-id'));
}

function annotate(previous_value, window_id){

    var previous_annotations_values = $('#my-data').attr('previous-annotations-values');
    var bootbox_title = "What can you see on the video? (Comma-separated annotations)";
    bootbox_title += "<p class='annotations-list-in-dialog'>Choices: " + previous_annotations_values + "</p>";

    bootbox.prompt({
      title: bootbox_title,
      value: previous_value,
      callback: function(ann) {
        if (check_annotation(ann)) {
            var data = {'window_id': parseInt(window_id), 'annotations': ann};
            $.post( "/annotatewindow", data)
                .done(function( returned_data ) {
                    $('#annotations-window-' + window_id).html(returned_data.annotations)
                });

        }
      }
    });

}

function delete_window(window_id){

    var data = {'window_id': parseInt(window_id)};
    $.post( "/deletewindow", data)
        .done(function( returned_data ) {
            $('#row-window-' + window_id).remove();
        });

}

function save_window_start_end_buffer(beg, end){
    current_window_start_end['beg'] = beg;
    current_window_start_end['end'] = end;
}

function start_window(){
    if($("button[device-selected='1']").length == 0){
        bootbox.alert("Please click on the device(s) the window corresponds to");
        return false;
    }
    devices_selected = [];
    window_start=get_current_time(true);
    $("button[device-selected='1']").each(function(){
        devices_selected.push(parseInt($(this).attr('device-id')));
    });
    console.log('start');
    $('#window-start-time-info').html('Window start: ' + window_start);
}

function stop_window(){
    if(window_start == -1){
        bootbox.alert("Please start a window first");
        return false;
    }
    if(window_start==get_current_time(true)){
        bootbox.alert("The window start and end times are the same!");
        return false;
    }

    var window_data = {
        'devices_selected': devices_selected,
        'video_start_time': parseInt(parseFloat(window_start) * 1000),
        'video_end_time': parseInt(parseFloat(get_current_time(true)) * 1000),
        'video_id': get_video_id(),
        'detection_type': 'manual'
    }
    console.log(window_data);

    $.post( "/createwindow", window_data)
        .done(function( data ) {
            console.log(data.tablerows.length)
            for(var i=0; i<data.tablerows.length; i++){
                console.log(data.tablerows[i]);
                add_annotation_row(data.tablerows[i]);
            }
        });

    window_start=-1;
    $('#window-start-time-info').html('');
}

$(document).ready(function(){

    $('.annotation-device-id').click(function(){
        $(this).toggleClass('btn-success');
        $(this).attr('device-selected', 1-parseInt($(this).attr('device-selected')));
    });

    $('#start-window').click(function(){
        start_window();
    });

    $('#stop-window').click(function(){
        stop_window();
    });

    $('.annotation-play').click(function(){
        var beg=$(this).attr('play-beg');
        var end=$(this).attr('play-end');
        var window_id = $(this)
        save_window_start_end_buffer(beg, end);
        update_window_id($(this));
        play_beg_end(beg, end);
    });

    $('.add-annotation').click(function(){
        annotate($(this).html(), $(this).attr('window-id'));
    });

    $('.delete-window').click(function(){
        delete_window($(this).attr('window-id'));
    });

    $("#annotations-table").tablesorter({sortList: [[4,0]]});

    if($('#my-data').attr('onstartplaywid') != -1){
        var wid = $('#my-data').attr('onstartplaywid');
        var beg = $('#play-window-id-' + wid).attr('play-beg');
        var end = $('#play-window-id-' + wid).attr('play-end');
        save_window_start_end_buffer(beg, end);
        play_beg_end(beg, end)
    }

    if($('#my-data').attr('playstarttime') != -1){
        var beg = $('#my-data').attr('playstarttime');
        var end = $('#my-data').attr('playendtime') == '-1' ? beg + 100 : $('#my-data').attr('playendtime');
        play_beg_end(beg, end);
    }

    $('#replay-window').click(function(){
        if(current_window_start_end['end'] != 0){
            play_beg_end(current_window_start_end['beg'], current_window_start_end['end'])
        }
    });

});
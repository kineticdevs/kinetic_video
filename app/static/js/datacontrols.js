// Data
00//'t': {'glyph': 'time', 'info': 'Synchronize video and data', 'var': 1},
00'datadim': {'glyph': 'menu-hamburger', 'info': 'List data dimensions', 'var': 0},
00'ws': {'glyph': 'menu-left', 'info': 'Start a window', 'var': 0},
00'we': {'glyph': 'menu-right', 'info': 'End and annotate a window', 'var': '+'},
00//'nadw': {'glyph': 'equalizer', 'info': 'List all non annotated data windows', 'var': 0},
00'pdw': {'glyph': 'expand', 'info': 'Play data window by ID', 'var': 1},
00'pw': {'glyph': 'unchecked', 'info': 'Display annotated window by ID', 'var': 1},
00//'dd': {'glyph': 'floppy-remove', 'info': 'Delete latest annotation for window if arg1=window OR point if arg1=point', 'var': 1},
00'movetimestamps': {'glyph': 'resize-horizontal', 'info': 'Play data window by ID', 'var': 1},
00'viz': {'glyph': 'picture', 'info': '(End and) Plot current window', 'var': '+'},
00//'toplot': {'glyph': 'list', 'info': 'Define variables to plot', 'var': '+'},
00'reloaddata': {'glyph': 'floppy-open', 'info': 'Reload data file', 'var': 0},
00'dwe': {'glyph': 'pencil', 'info': 'Annotate a data window', 'var': '+'},
00'stream': {'glyph': 'stats', 'info': 'Toogle streaming visualization', 'var': '+'},
'reprocess': {'glyph': 'th-large', 'info': 'Reprocess data file', 'var': '+'},
'createdetectionwindows': {'glyph': 'export', 'info': 'Create detection windows', 'var': 0},
00//'g': {'glyph': 'export', 'info': 'Generate data file from window', 'var': 0},

from flask.ext.wtf import Form
from wtforms import StringField, TextAreaField
from wtforms.validators import DataRequired


class RegisterVideo(Form):
    location = StringField('location', validators=[DataRequired()])
    day = StringField('day', validators=[DataRequired()])
    absolute_filepath = StringField('absolute_filepath', validators=[DataRequired()])
    devices_ids = StringField('devices_ids', validators=[DataRequired()])
    description = TextAreaField('description', validators=[DataRequired()])
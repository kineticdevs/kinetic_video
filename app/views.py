import os

from flask import render_template, flash, redirect, request, jsonify

from app import app, db, models
from .forms import RegisterVideo
from services.helper import upload_to_s3, generate_url_valid_for, upload_annotations_to_s3

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')


@app.route('/registervideo', methods=['GET', 'POST'])
def registervideo():
    form = RegisterVideo()
    if form.validate_on_submit():

        if not os.path.isfile(form.absolute_filepath.data):
            flash('The file {} does not exist. Please verify the path.'.format(form.absolute_filepath.data), 'danger')
            return render_template('registervideo.html', form=form)

        upload_success, s3_key = upload_to_s3(location=form.location.data,
                                              day=form.day.data,
                                              absolute_filepath=form.absolute_filepath.data)

        if not upload_success:
            flash('The file was not uploaded to S3 as the key {} already exists'.format(s3_key), 'warning')
            return render_template('registervideo.html', form=form)

        # Commit to database
        video_in_db = models.Video(location=form.location.data,
                                   day=form.day.data,
                                   s3_key=s3_key,
                                   description=form.description.data)
        db.session.add(video_in_db)
        db.session.commit()

        print form.devices_ids.data
        for device_id in form.devices_ids.data.split(','):
            video_device = models.VideoDevice(video_id=video_in_db.id,
                                              device_id=int(device_id))
            db.session.add(video_device)
        db.session.commit()

        flash('Video {} added to S3 successfully! Its key is {}'.format(form.absolute_filepath.data, s3_key), 'success')
        return redirect('/index')

    return render_template('registervideo.html', form=form)


@app.route('/loadvideo', methods=['GET', 'POST'])
def loadvideo():
    return render_template('loadvideo.html', videos_from_s3=models.Video.query.all())

@app.route('/annotations', methods=['GET', 'POST'])
def annotations():
    windows = models.Window.query.all()

    return render_template('annotations.html', windows=windows)


@app.route('/playvideo', methods=['GET', 'POST'])
def playvideo():
    video_id = request.args.get("video_id")

    onstartplaywid = request.args.get('onstartplaywid') if 'onstartplaywid' in request.args else -1

    playstarttime = request.args.get('playstarttime') if 'playstarttime' in request.args else -1
    playendtime = request.args.get('playendtime') if 'playendtime' in request.args else -1

    previous_annotations_values = ','.join([a.value for a in models.Annotation.query.all()])

    annotations = []
    # Create the list of all videos with windows
    for videodevice in models.VideoDevice.query.filter(models.VideoDevice.video_id==video_id).all():
        for window in videodevice.windows:
            tablerow = {'window_id': window.id,
                        'video_window_id': window.video_window.id,
                        'video_s3_key': videodevice.video.s3_key,
                        'video_id': videodevice.video_id,
                        'start_t': window.video_window.start_t / 1000.0,
                        'end_t': window.video_window.end_t / 1000.0,
                        'd_type': window.video_window.detection_type,
                        'annotations': window.get_annotations_values(),
                        'device_id': window.video_device.device_id}
            annotations.append(tablerow)

    video = models.Video.query.get(video_id)

    return render_template('playvideo.html',
                           s3_video_url=generate_url_valid_for(video.s3_key),
                           video=video,
                           annotations=annotations,
                           onstartplaywid=onstartplaywid,
                           playstarttime=playstarttime,
                           playendtime=playendtime,
                           previous_annotations_values=previous_annotations_values)


@app.route('/sync', methods=['GET', 'POST'])
def sync():
    try:
        video_id = request.form["video_id"]
        video = models.Video.query.get(video_id)
        video.sync(current_video_time_ms=int(request.form['current_video_time_ms']),
                   true_time=str(request.form['true_time']),
                   video_length_ms=int(request.form['video_length_ms']))
        db.session.commit()
    except ValueError:
        flash('Wrong format, please enter', 'danger')
    return ''

@app.route('/createwindow', methods=['GET', 'POST'])
def createwindow():
    tablerows = []
    video_window = models.VideoWindow(start_t=request.form['video_start_time'],
                                      end_t=request.form['video_end_time'],
                                      detection_type=request.form['detection_type'],
                                      video_id=request.form['video_id'])
    db.session.add(video_window)
    db.session.commit()

    for device_id in request.form.getlist("devices_selected[]"):
        videodevice = models.VideoDevice.query.filter(models.VideoDevice.device_id == device_id,
                                                          models.VideoDevice.video_id == int(request.form['video_id'])).first()
        new_window = models.Window(video_device_id=videodevice.id,
                                   video_window_id=video_window.id)
        db.session.add(new_window)
        db.session.commit()

        tablerows.append({'window_id': new_window.id,
                          'video_s3_key': videodevice.video.s3_key,
                          'video_id': videodevice.video_id,
                          'start_t': video_window.start_t / 1000.0,
                          'end_t': video_window.end_t / 1000.0,
                          'd_type': video_window.detection_type,
                          'annotations': new_window.get_annotations_values(),
                          'device_id': new_window.video_device.device_id})

    return jsonify(tablerows=tablerows)


@app.route('/annotatewindow', methods=['GET', 'POST'])
def annotatewindow():
    window_id = request.form['window_id']
    window = models.Window.query.get(window_id)
    annotations_values = map(lambda s: s.replace(' ',''), set(request.form['annotations'].split(',')))

    # Delete all annotations for that window
    # Create new AnnotationWindow entry mapped to that Annotation
    for aw in models.AnnotationWindow.query.filter(models.AnnotationWindow.window_id == window_id).all():
        db.session.delete(aw)
    db.session.commit()

    # (Creating and) Getting Annotation id for each annotation_value
    for av in annotations_values:

        # Deal with the annotation value (Annotation table)
        query_result = models.Annotation.query.filter(models.Annotation.value == av).all()
        exists = len(query_result) > 0
        if not exists:
            new_annotation = models.Annotation(value=av)
            db.session.add(new_annotation)
            db.session.commit()
            annotation_id = new_annotation.id
        else:
            annotation_id = query_result[0].id

        new_annotation_window = models.AnnotationWindow(window_id=window_id, annotation_id=annotation_id)
        db.session.add(new_annotation_window)
        db.session.commit()

    db.session.commit()
    return jsonify(annotations=window.get_annotations_values())

@app.route('/deletewindow', methods=['GET', 'POST'])
def deletewindow():
    window_id = request.form['window_id']
    window = models.Window.query.get(window_id)

    #First, delete all annotations_windows mapped to that window
    annotation_windows = models.AnnotationWindow.query.filter(models.AnnotationWindow.window_id == window_id)
    for aw in annotation_windows:
        db.session.delete(aw)
    db.session.commit()

    # Then delete the window
    db.session.delete(window)
    db.session.commit()

    return ''


def _downloadfromredshift(video_id, force_dl=False):
    from cache import Cache
    devices_ids = [vd.device_id for vd in models.VideoDevice.query.filter(models.VideoDevice.video_id == video_id).all()]
    for device_id in devices_ids:
        cache = Cache(device_id=device_id, video_id=video_id)
        cache.download_from_redshift(force_dl=force_dl)

@app.route('/downloadfromredshift/<video_id>', methods=['GET', 'POST'])
def downloadfromredshift(video_id):
    _downloadfromredshift(video_id, force_dl=True)
    return redirect('/loadvideo')


def get_annotations_header_row():
    return ['annotation_window_id',
            'window_id',
            'video_window_id',
            'annotation',
            'video_start_t',
            'timestamp_data_start_t',
            'video_end_t',
            'timestamp_data_end_t',
            'video_id',
            'device_id',
            's3_key']


@app.route('/loadredshift', methods=['GET', 'POST'])
def loadredshift():

    data = list()

    for aw in models.AnnotationWindow.query.all():
        point = list()
        point.append(aw.id)
        point.append(aw.window.id)
        point.append(aw.window.video_window.id)
        point.append(aw.annotation.value)
        point.append(aw.window.video_window.start_t)
        point.append(aw.window.video_window.start_t + int(aw.window.video_window.video.start_t))
        point.append(aw.window.video_window.end_t)
        point.append(aw.window.video_window.end_t + int(aw.window.video_window.video.start_t))
        point.append(aw.window.video_device.video.id)
        point.append(aw.window.video_device.device_id)
        point.append(aw.window.video_device.video.s3_key)
        data.append(point)

    rows = list()
    rows.append(','.join(get_annotations_header_row()))  # header row
    for point in data:
        rows.append(','.join(map(lambda x: str(x), point)))

    string = '\n'.join(rows)

    upload_annotations_to_s3(string)

    from kinetic_video.aws_sqs import SQS
    import json
    ANNOTATIONS_KEY = 'annotations/tmp.csv'
    payload = {
        "process": "append",
        "transformer": "Annotations",
        "source_files": [
            "s3://kinetic-video/{}".format(ANNOTATIONS_KEY),
            ],
        "target_schema": "dw",
        "target_table": "annotations"
    }

    SQS(aws_region='us-east-1',
        aws_key=os.environ['AWS_KEY'],
        aws_secret=os.environ['AWS_SECRET'],
        aws_queue=os.environ['AWS_QUEUE_NAME']).push(json.dumps(payload))

    flash('1. Data flattened and sent to S3. 2. Payload sent to the queue. It will be soon available in RedShift','success')

    return redirect('/index')


@app.route('/cleanannotations', methods=['GET', 'POST'])
def cleanannotations():

    cleaned_annotations = set([aw.annotation.value for aw in models.AnnotationWindow.query.all()])

    for a in models.Annotation.query.all():
        if a.value not in cleaned_annotations:
            db.session.delete(a)
    db.session.commit()

    flash('Annotations cleaned', 'success')

    return redirect('/index')

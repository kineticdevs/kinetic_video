# README #

You'll need to run a local version of the app on your laptop in order to open the video. The steps are the following:

1. git clone repo kinetic_video
2. cd into kinetic_video
3. virtualenv .
4. source bin/activate
5. pip install -r requirements.txt
6. export AWS_KEY and export AWS_SECRET, also export AWS_QUEUE_NAME=redshift-loader and export ANNOTATIONS_DATABASE_URI=postgres://kinetic:1MMatrga@labels.czftpff8xgdz.us-east-1.rds.amazonaws.com:5432/production
7. python application.py
8. a link to where it's running on your localhost will be printed
9. click on it and go to register a new video
10. enter the info and local path of video. Note the Day is in the format: YYYYMMDD

ALSO please compress the video before uploading it. Compress it with 200kbps please.








------------ OLD README ------------
### What is this repository for? ###

Kinetic Video enables you to:

* control a video
* add and play with a corresponding annotations file in JSON format
* add and play with a corresponding data file record in Pandas DataFrame CSV export format
on a VLC Media Player instance via the console, using a set of pre-defined commands.

### How do I get set up? ###

#### Installation
* Install VLC Media Player
* Python 32 bits with SciPy stack (pandas, numpy, scipy). For Windows users install Anaconda 32 bits. It DOES NOT work on 64 bit with the VLC Python bindings since VLC is a 32 bit program!
* Ideally create a Kinetic folder somewhere on your machine. Clone the kinetic_video Bitbucket repo in Kinetic directory.
* Download the data from Google Drive in that Kinetic folder.

Linux run 
sudo apt-get install libpng-dev, libjpeg8-dev, libfreetype6-dev
sudo apt-get install git gfortran g++
sudo apt-get install libatlas-base-dev liblapack-dev

and then pip install requirements.txt should work.

You are now ready to play!

#### How to start
* Open a command prompt/terminal and go to Kinetic/. Now run python kinetic_video/annotate.py -h. It will tell you which arguments to give to the app. 

Just run python kinetic_video/annotate.py arg1 arg2 arg3 arg4 where the arguments are:

* arg1: RELATIVE_PATH_TO_VIDEO_DIRECTORY
* arg2: VIDEO_FILE_NAME_WITH_EXTENSION
* arg3: DATAFILE_DIRECTORY
* arg4: DATAFILE_NAME_WITH_CSV_EXTENSION

Please don't forget the / at the end of the directories paths.

#### Example
python kinetic_video/annotate.py data1/ video1.mp4 data1/ prep_XX_XX.csv 


#### Tested on
* Ubuntu 14.04
* Windows 7/8

#### Directories
Three types of files are used: videos (.mp4, .mkv or other), annotations (.json), data files (recorded our devices and then processed) (.csv). The videos and annotations must be in the same folder, if a video is named X.EXTENSION then the corresponding annotation file this app will look for must be named X.EXTENSION.annotation.json.


### A short tutorial

1. First of all, follow the installation steps described above.
2. Follow the How to start steps described above.
3. At the beginning of the video the UTC time must be displayed (like someone is filming a phone showing it or something). Press <r_0.3>, it will set the video rate to 0.3. When the UTC time goes from one second to the next one press the space bar. It will pause the video. Then press "i". If the time displayed in the console/terminal (in your local timezone) is the same as the UTC time displayed on the video, then you are lucky the video is already synchronized. Otherwise type the UTC time you see on the video with the following format <YYYY/MM/DD HH/MM/SS>. You have now a video synced with your data!
4. Press the space bar to pause/play the video.
5. Press <r_1.0> to reset the video rate to be 1.0. You can play with the rate value as long as it stays between 0.2 and 2.0.
6. Press w to go one second forward, q for one second backward.
7. Now you want to save a window on the video, so press <ws> exactly when the window starts (if you want to be accurate you can pause the video and then type <ws>). "ws" stand for "window start". Then play or navigate the video (using w or q) until when you want to end that window.
8. Now you've reached the end of your window you have two options: press <we_arg1_arg2_..._argN> where arg1, ... argN are all the labels you want to give to that window. "we" stand for "window end". That command will save the data for this window in your /data/ directory, and will save a plot associated with this data.
9. Second option is just to visualize this data, so you do first <toplot_arg1_..._argN> where all the arguments separated by an underscore are going to be the names of the variables you want to plot over that window. These variables must appear in the data file CSV. Then type <viz> to save the plot in /data/. Depending on the OS it will also directly open the plot file (.png).
10. If you have saved the window data using the <we> command at step 8, then you can play that window again and again by typing <pw_ID> where ID is this window ID. It will be displayed just after you types the <we> command. You can navigate through these windows by typing <pw_+> (going to the next window) and <pw_-> (going to the previous one). Also typing <replay> will replay the current window. If you are on a window (i.e. you just replayed it), then typing <viz> will save the corresponding plot.
11. This is it for today! Press x to close the app.


### How does it work?
Once it runs a VLC window will open and play the video, a bunch of commands can be executed from the terminal. They'll allow you to control the video. There are two types of commands:

* the short ones, executable with one key (such as PLAY/PAUSE triggered by the space bar, i.e. the key " ", or x to quit the app)
* the long ones, such as PLAY ANNOTATION WINDOW NUMBER 4. That would be launched by typing <pw_4>.

#### List of short commands
* "?" - Display command cheat sheet
* " " - Play/Pause the player
* "i" - Display information about current time and annotations
* "a" - Display all the currently annotated windows and points
* "x" - Force the app to terminate
* "m" - Mute
* "/" - Restart video
* "s" - Save annotations
* "q" - Move by one second backwards
* "w" - Move by one second forward
* "v" - Finds closest value in the data. Currently prints hard-coded values of sagittal angle, elevation angle of the arm and adjusted wrist height. If you need to change the variables to print, look for "print 'Values at this time:'" in vlc_wrapper.py and modify this line below by adding/removing variable names that you can find in the data file header.
* "*" - Equivalent of backspace to erase last typed key. Example I type "<hellos" and then press "*" it will display "<hello"

#### List of long commands
Long commands must be enclosed by <>. Arguments are separated by underscore "_". For example writing "<kl_1.0_XX>" will call the command "kl" with first parameter "1.0" and second parameter "XX".

* "mb_arg1" - Move video by +- arg1 (int) milliseconds
* "r_arg1" - Set video rate to arg1 (float between 0.1 and 2.0) 
* "t_arg1" - Set start timestamp. Stop the video at the beginning, when it is filming the UTC time on the phone. Then use this command with arg1 in the following format YYYY/MM/DD HH/MM/SS (UTC time).
* "p_arg1_arg2_..._argN" - Stop the video and annotate a point with as many arguments as you want. Example: <p_v_10.0> for vertical position of the arms at 10.0inches for instance.
* "ws" - Start a window to annotate
* "we" - Mark a window end and annotate it. Example: after having started the window with the <ws> at the beginning of an action, when the action you want to annotate is over, pause and press <we_twist_mlm> if you want to annotate a twist from middle to left to middle (mlm). The coresponding window of data is saved into a folder /data/ in your video directory.
* "pw" - Display annotated window. The arguments can be either "+" to play the next window, "-" to play the previous window, or the window ID (e.g. 10). Example: play Ith annotated window with command <pw_I>, play next window <pw_+>.
* "dd" - Delete latest annotation for window if arg1=window OR point if arg1=point'
* "pws" - Play all annotated windows in a loop (not highly recommended but implemented °_°!)
* "replay" - to replay the latest window
* "toplot" - Defines the variables to plot when the command "viz" is launched, on the previous selected window. For instance typing <toplot_variableZ_variableY_variableX> would set the variables to plot to be variableX, variableY and variableZ.
* "viz" - Saves in /data/ a plot of the variables set by the command "toplot" above (versus time in ms). 

### Annotations format

The files in the GGDocs are videos + annotations (json) + data files (csv). The annotation format is the following:

* timestamps: absolute timestamps in milliseconds UTC Time (datetime.datetime.fromtimestamp in Python to get the date) for the start and end of the video. They should match timestamps in the datafiles.
* window_annotations: a list of each window_annotation (see below)
* window_annotation: the window start and end times (times from the beginning of the video). If you need the absolute UTC timestamp, just add the video start UTC timestamp from the timestamps entry. And that will match the timestamps in the data. It also has the entry "value" which is a list of all the annotations. "st" stands for "standing", the consecutive letters "mrl" stand for "middle, right, left". When "twist" or "turn" is present in the "value", coupled with "mrl", that means the person is twisting or turning from a middle position, to the right side and finishes on the left side (That helps understanding which rotations have been performed).
 

### Who do I talk to?

* selim@wearkinetic.com
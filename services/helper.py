"""
Helper functions
"""
import boto

BUCKET_NAME = 'kinetic-video'
ONE_HOUR = 60 * 60
ONE_DAY = 24 * 60 * 60
ANNOTATIONS_KEY = 'annotations/tmp.csv'

def print_dict_nicely(d):
    for key in d.keys():
        print key, '=', d[key]


def get_key_from_full_path(fullpath, with_extension=False):
    ret = fullpath.split('/')[-1]
    if not with_extension:
        ret = ret.split('.')[0]
    return ret


def get_extension_from_path(path):
    return path.split('.')[-1]


def locate_video_on_s3(location, day, key_id=0, extension='x'):
    folder = '{}/{}/'.format(location, day)
    return {'folder': folder,
            'key_id': key_id,
            'extension': extension.lower(),
            'key': '{}{}.{}'.format(folder, key_id, extension)}


def find_new_key_on_s3(location, day, mybucket=None):
    if mybucket is None:
        # Connect to boto
        conn = boto.connect_s3()
        BUCKET_NAME = 'kinetic-video'
        mybucket = conn.get_bucket(BUCKET_NAME)

    geography_s3 = locate_video_on_s3(location=location, day=day)
    all_keys = [k.key for k in mybucket.get_all_keys(prefix=geography_s3['folder'])]
    print all_keys
    all_keys_ids = [int(get_key_from_full_path(k)) for k in all_keys if get_key_from_full_path(k) != '']
    print all_keys_ids
    new_key_id = max(all_keys_ids) + 1 if len(all_keys_ids) > 0 else 1

    return new_key_id


def upload_to_s3(location, day, absolute_filepath):
    import os
    conn = boto.connect_s3(aws_access_key_id=os.environ['AWS_KEY'],
                           aws_secret_access_key=os.environ['AWS_SECRET'])
    mybucket = conn.get_bucket(BUCKET_NAME)

    # Key object on this bucket
    k = boto.s3.key.Key(mybucket)

    key = locate_video_on_s3(location=location,
                             day=day,
                             key_id=find_new_key_on_s3(location=location, day=day, mybucket=mybucket),
                             extension=get_extension_from_path(absolute_filepath))

    # Double check this new key does not exist
    if mybucket.get_key(key['key']):
        print 'The key {} already exists'.format(key['key'])
        return False, key['key']

    # Name key, set metadata and upload to S3
    k.key = key['key']
    k.set_metadata('absolute_filepath_on_upload', absolute_filepath)
    k.set_contents_from_filename(absolute_filepath, replace=False)

    return True, key['key']


def upload_annotations_to_s3(string):
    # Connect to boto
    import os

    conn = boto.connect_s3(aws_access_key_id=os.environ['AWS_KEY'],
                           aws_secret_access_key=os.environ['AWS_SECRET'])
    mybucket = conn.get_bucket(BUCKET_NAME)

    # Key object on this bucket
    k = boto.s3.key.Key(mybucket)

    # Name key, set metadata and upload to S3
    k.key = ANNOTATIONS_KEY
    k.set_contents_from_string(string)

    return True, ANNOTATIONS_KEY


def generate_url_valid_for(key, default=ONE_DAY):
    import os
    conn = boto.connect_s3(aws_access_key_id=os.environ['AWS_KEY'],
                           aws_secret_access_key=os.environ['AWS_SECRET'])
    mybucket = conn.get_bucket(BUCKET_NAME)
    k = mybucket.get_key(key)
    return k.generate_url(expires_in=default, query_auth=True, force_http=True)


def unload_from_redshift(device_id, start_t, end_t):
    import urlparse
    import os
    import psycopg2
    from pandas import read_sql

    connection_params = urlparse.urlparse(os.environ['REDSHIFT_CONNECTION_STRING'])
    username = connection_params.username
    password = connection_params.password
    database = connection_params.path[1:]
    port = connection_params.port
    hostname = connection_params.hostname

    redshift_connection = psycopg2.connect(
        database=database,
        user=username,
        password=password,
        host=hostname,
        port=port)


    timestamp_alias = '(extract(epoch from log_timestamp)::bigint * 1000) + extract(ms from log_timestamp)'

    from emr_runner import time_utilities as tutil
    from datetime import datetime as dt

    query = "select counter_back as counterBack, counter_wrist as counterWrist, accel_back_x as accelXBack, accel_back_y as accelYBack, accel_back_z as accelZBack, accel_wrist_x as accelXWrist, accel_wrist_y as accelYWrist, accel_wrist_z as accelZWrist, quat_back_w as quatWBack, quat_back_x as quatXBack, quat_back_y as quatYBack, quat_back_z as quatZBack, quat_wrist_w as quatWWrist, quat_wrist_x as quatXWrist, quat_wrist_y as quatYWrist, quat_wrist_z as quatZWrist, gyro_back_x as gyroXBack, gyro_back_y as gyroYBack, gyro_back_z as gyroZBack, gyro_wrist_x as gyroXWrist, gyro_wrist_y as gyroYWrist, gyro_wrist_z as gyroZWrist, temp_back as tempBack, temp_wrist as tempWrist, delta_height_back as deltaHeightBack, delta_height_wrist as deltaHeightWrist, device_name as deviceId, gravity_back_x as gravityXBack, gravity_back_y as gravityYBack, gravity_back_z as gravityZBack, gravity_wrist_x as gravityXWrist, gravity_wrist_y as gravityYWrist, gravity_wrist_z as gravityZWrist, sagittal_angle as sagittalAngle, raw_file_path as raw_file_path, decoded_file_path as decoded_file_path, "
    query += "{} as timestamp ".format(timestamp_alias)
    query += "from dw.device_logs "
    query += "where log_timestamp between '{}' and '{}' and device_name = '{}' ".format(
        tutil.datetime_to_string(dt.utcfromtimestamp(int(start_t) / 1000)),
        tutil.datetime_to_string(dt.utcfromtimestamp(int(end_t) / 1000)),
        device_id)

#    cur = redshift_connection.cursor()
#    cur.execute(query)

#    return cur.fetchall()
    return read_sql(sql=query, con=redshift_connection)


def get_annotations_header_row():
    return ['annotation_window_id',
            'window_id',
            'annotation',
            'video_start_t',
            'data_start_t',
            'video_end_t',
            'data_end_t',
            'video_id',
            'device_id',
            's3_key']

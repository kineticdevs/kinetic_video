import os

from flask import flash

from app import models
from services.helper import unload_from_redshift


HARDCODED_CACHE_FOLDER = '/home/selim/cache'

class MultipleCache:

    def __init__(self, devices_ids, video_id):
        self.devices_ids = devices_ids
        self.video_id = video_id
        self.caches = dict()

    def get_caches(self):
        for device_id in self.devices_ids:
            self.caches[device_id] = Cache(device_id, self.video_id)

    def load_caches(self):
        for device_id in self.devices_ids:
            self.caches[device_id].load()


class Cache:

    def __init__(self, device_id, video_id):

        self.video = models.Video.query.get(video_id)
        self.device_id = device_id

        self.directory = HARDCODED_CACHE_FOLDER
        if not os.path.exists(self.directory):
            os.makedirs(self.directory)

        self.data = []

    def load(self):
        if not self.download_from_redshift():
            return False

        self.data = []

    def is_cacheable(self):
        return not (self.video.start_t is None)

    def get_file_path(self, f_name, extension):
        return '{}/{}.{}'.format(self.directory, f_name, extension)

    def get_file_path_unload_redshift(self):
        return self.get_file_path(f_name='kinetic-{}_video-{}'.format(self.device_id, self.video.id),
                                  extension='csv')

    def is_already_cached(self):
        return os.path.isfile(self.get_file_path_unload_redshift())

    def download_from_redshift(self, force_dl=False):
        if not self.is_cacheable():
            if force_dl:
                flash('Please synchronize this video before attempting to download the associated data', 'danger')
            return False

        if self.is_already_cached():
            if force_dl:
                flash('Data for device {} and video {} is already cached locally in {}'.format(self.device_id, self.video.id, self.directory), 'success')
            return True

        unload_from_redshift(self.device_id, self.video.start_t, self.video.end_t).\
            sort('timestamp', inplace=False).\
            reset_index(drop=True, inplace=False).\
            to_csv(self.get_file_path_unload_redshift())

        flash('Data for device {} and video {} cached locally in {}'.format(self.device_id, self.video.id, self.directory), 'success')
        return True

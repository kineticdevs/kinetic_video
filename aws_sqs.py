from boto.sqs import connect_to_region
from boto.sqs.message import RawMessage


class SQS():
    def __init__(self, aws_region, aws_key, aws_secret, aws_queue):
        self.sqs_conn = connect_to_region(aws_region, aws_access_key_id=aws_key, aws_secret_access_key=aws_secret)
        self.sqs_queue = self.sqs_conn.get_queue(aws_queue)

    def push(self, message):
        msg = RawMessage()
        msg.set_body(message)
        self.sqs_queue.write(msg)

    def get(self):
        return self.sqs_queue.read()

    def delete(self, message):
        self.sqs_queue.delete_message(message)
